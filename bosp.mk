
ifdef CONFIG_EXTERNAL_GRPC

MODULE_DIR_GRPC = external/optional/grpc

GIT_REPO_SSH    = git@github.com:grpc/grpc.git
GIT_REPO_HTTPS  = https://github.com/grpc/grpc.git

GRPC_PROJECT_DIR  = grpc
GRPC_PROJECT_PATH = $(MODULE_DIR_GRPC)/$(GRPC_PROJECT_DIR)
GRPC_REF_COMMIT   = v1.7.2

PROTOBUF_PROJECT_PATH = $(GRPC_PROJECT_PATH)/third_party/protobuf

external: grpc
clean_external: clean_grpc


.PHONY: build_grp clean_grpc uninstall_grpc

GIT_REPO_EXIST=$(shell [ -d $(GRPC_PROJECT_PATH)/.git ] && echo 1 || echo 0)

grpc: build_grpc

download_grpc:
	@echo "                                         "
	@echo "======= Downloading Google gRPC I/O ====="
ifeq ($(GIT_REPO_EXIST), 1)
	$(info "------ gRPC project already cloned -----")
else
	@git clone $(GIT_REPO_HTTPS) $(GRPC_PROJECT_PATH)
	@cd $(GRPC_PROJECT_PATH) && \
		git submodule update --init && \
		git checkout $(GRPC_REF_COMMIT) && \
		git submodule update
endif
	@echo "========================================="


build_grpc: download_grpc
	@echo "                                         "
	@echo "======= Building Google gRPC I/O ========"
	@echo " Using GCC    : $(CC)"
	@echo " Target flags : $(TARGET_FLAGS)"
	@echo " Sysroot      : $(PLATFORM_SYSROOT)"
	@echo " Host         : $(PLATFORM_TARGET)"
	@echo " Install to   : $(BUILD_DIR)"
	@echo " BOSP Options : $(CMAKE_COMMON_OPTIONS)"
	@echo "========================================="
	@cd $(GRPC_PROJECT_PATH) && \
		make -j $(CPUS) install prefix=$(BUILD_DIR)
	@cd $(PROTOBUF_PROJECT_PATH) && \
		make -j $(CPUS) install prefix=$(BUILD_DIR)

clean_grpc:
	@echo "                                         "
	@echo "======= Cleaning Google gRPC I/O ========"
	@cd $(GRPC_PROJECT_PATH) && make clean
	@cd $(PROTOBUF_PROJECT_PATH) && make clean
	@echo "========================================="

uninstall_grpc: clean_grpc
	@echo "                                         "
	@echo "======= Uninstalling Google gRPC I/O ===="
	@rm -rf $(BUILD_DIR)/include/grpc*
	@rm -rf $(BUILD_DIR)/lib/libgrpc*
	@rm -rf $(BUILD_DIR)/lib/libgrp*
	@echo "========================================="

endif # CONFIG_EXTERNAL_GRPC
